# coding: utf-8
import os
import re
import csv
import codecs
import string
import datetime
import lxml.html
from selenium import webdriver
from bs4 import BeautifulSoup
from time import sleep

global now
def time_crawl(p_name, p_url):
    # 1
    # driver = webdriver.PhantomJS()
    driver = webdriver.PhantomJS(executable_path = "/usr/local/bin/phantomjs")
    driver.get(p_url)
    data = driver.page_source.encode('utf-8')
    soup = BeautifulSoup(data, "lxml")

    now = datetime.datetime.now()
    print(now)
    print(p_name)
    f = open('output.csv', 'w')
    f.write("駐車場名,満空情報,車室数\n")
    writer = csv.writer(f, lineterminator='\n')

    name_list = soup.find_all("div",class_="s_ichiran_info_name ellipsis")
    manku_list = soup.find_all("div",class_="s_ichiran_info_manku")
    shitsu_list = soup.find_all("span",class_="s_ichiran_info_count")

    for (name, manku, shitsu) in zip(name_list, manku_list, shitsu_list):
        # print (name.text) + "," + (manku.text) + "," + (shitsu.text)
        csvlist = []
        csvlist.append(name)
        csvlist.append(manku)
        csvlist.append(shitsu)
        writer.writerow(csvlist)
    f.close()

    # 2
    f2 = codecs.open("output.csv","r")
    out_file = codecs.open('/home/ubuntu/crawl/outputs/Times_' + p_name + '_{0:%Y%m%d_%H:%M:%S}.csv'.format(now),"w")
    # out_file = codecs.open('Times_' + p_name + '_{0:%Y%m%d_%H:%M:%S}.csv'.format(now),"w")
    lines = f2.readlines()

    for line in lines:
        line = line.replace("\n","")
        line = line.split(",")
        row = "{},{},{}\n".format(
            line[0].translate(None, '<div class="s_ichiran_info_name ellipsis">/'),
            line[1].translate(None, '<div class=s_ichiran_info_name ellipsis>/"kuw'),
            line[2].translate(None, '<span class="s_ichiran_info_count">/'),
            )
        out_file.write(string.join(row,''))
    f2.close()
    out_file.close()
    os.remove("./output.csv")

    # 3    jpに変換 修正予定
    f_in  = codecs.open('/home/ubuntu/crawl/outputs/Times_' + p_name + '_{0:%Y%m%d_%H:%M:%S}.csv'.format(now), 'r', 'utf-8')
    # f_in  = codecs.open('Times_' + p_name + '_{0:%Y%m%d_%H:%M:%S}.csv'.format(now), 'r', 'utf-8')
    f_out = codecs.open('jp.csv', 'w', 'utf-16')
    lines = f_in.readlines()
    lines2 = []
    for line in lines:
        lines2.append(line)
    else:
        f_out.write(string.join(lines2,''))
        f_in.close()
        f_out.close()

def main():
    parks = {
        1 : {
            'name' : 'kanda',
            'url' : 'https://times-info.net/P13-tokyo/line/L2100/S1923/'
        },
        2 : {
            'name' : 'shimo',
            'url' : 'https://times-info.net/P13-tokyo/line/L13700/S2281/'
        },
        3 : {
            'name' : 'harajuku',
            'url' : 'https://times-info.net/P13-tokyo/line/L5300/S2832/'
        },
        4 : {
            'name' : 'iida',
            'url' : 'https://times-info.net/P13-tokyo/line/L2500/S1533/'
        },
        5 : {
            'name' : 'shibuya',
            'url' : 'http://times-info.net/P13-tokyo/C113/'
        }
    }

    for i in range(len(parks)):
        n = parks[i+1]['name']
        u = parks[i+1]['url']
        time_crawl(n,u)
        sleep(1)
    return

if __name__ == '__main__':
    main()
